from django.contrib import admin
from smartappoint.models import Doctor, Patient, Specialty, DoctorAddress, DoctorAgenda, Appointment, PatientDelay, DoctorDelay

# Register your models here.
admin.site.register(Doctor)
admin.site.register(Patient)
admin.site.register(Specialty)
admin.site.register(DoctorAddress)
admin.site.register(DoctorAgenda)
admin.site.register(Appointment)
admin.site.register(PatientDelay)
admin.site.register(DoctorDelay)
