from smartappoint.models import Doctor, Patient, Specialty, DoctorAddress, DoctorAgenda, Appointment, PatientDelay, DoctorDelay
from rest_framework import serializers

class SpecialtySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Specialty
        fields = ('id', 'code', 'description')

class DoctorSerializer(serializers.HyperlinkedModelSerializer):
    specialty = SpecialtySerializer()

    class Meta:
        model = Doctor
        fields = ('id', 'register', 'name', 'specialty')

class PatientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Patient
        fields = ('id', 'document', 'name', 'address', 'phone_number', 'email')

class DoctorAddressSerializer(serializers.ModelSerializer):
    doctor = DoctorSerializer()

    class Meta:
        model = DoctorAddress
        fields = ('id', 'doctor', 'description', 'address', 'phone_number', 'email')

class DoctorAgendaSerializer(serializers.ModelSerializer):
    doctor_address = DoctorAddressSerializer()

    class Meta:
        model = DoctorAgenda
        fields = ('id', 'doctor_address', 'date', 'start', 'end', 'busy')

class AppointmentSerializer(serializers.HyperlinkedModelSerializer):
    doctor_address = DoctorAddressSerializer()
    patient = PatientSerializer()

    class Meta:
        model = Appointment
        fields = ('id', 'doctor_address', 'patient', 'date', 'start', 'end', 'effective_start')

class PatientDelaySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PatientDelay
        fields = ('id', 'appointment', 'amount')

class DoctorDelaySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DoctorDelay
        fields = ('id', 'appointment', 'amount')
